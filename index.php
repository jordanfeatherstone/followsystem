<html>
<head>
<link rel="stylesheet" type="text/css" href="style.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<?php include 'connect.php';?>
</head>
<body>
	<div class="container">
		<button id="btnfollow" class="btn followButton" name="flwbtn" rel="2">Follow</button>
	</div>
	<?php 
	/* 
	Above is the button
	Below is the if statement that will check if the user is already following them 
	and show the correct style of button depending on if they are or not
	
	user_id='1' and follower_id='2' - the user_id would be the person logged in
	follwer is the id of the person they are trying to follow
	*/
	
	
	$result = $conn->query("SELECT * FROM follow where user_id='1' and follower_id='2'");
	if($result->num_rows > 0) { 
		echo 'if result do this';?>
		<script>	
				$('button.followButton').addClass('following').text('Following');		
		</script><?php
	} else { echo 'no result';?>
		<script>

		</script><?php	
	}
	$one = 1;
	$two = 2;
	?>

	
	<script>
	// The rel attribute is the userID you would want to follow
	$('button.followButton').click(function(e){
		
		e.preventDefault();
		$button = $(this);
		 var $user_id = "<?php echo $one ?>";
		 var $follower = "<?php echo $two ?>"; 
		if($button.hasClass('following')){
			
			//$.ajax(); Do Unfollow
			$.ajax({
			type: "POST",
			url: "unfollow.php",
			dataType: 'html',
			data: {user_id: $user_id, follower: $follower},
			success: function(){
						/* Remove the alert when you got it working they where just for tests */			
				}
			});

			$button.removeClass('following');
			$button.removeClass('unfollow');
			$button.text('Follow');
		} else {
			
			// $.ajax(); Do Follow
			$.ajax({
			type: "POST",
			url: "follow.php",
			dataType: 'html',
			data: {user_id: $user_id, follower: $follower},
			success: function(){
						/* Remove the alert when you got it working they where just for tests */
					
				}
			});
			
			$button.addClass('following');
			$button.text('Following');
		}
	});

	$('button.followButton').hover(function(){
		 $button = $(this);
		if($button.hasClass('following')){
			$button.addClass('unfollow');
			$button.text('Unfollow');
		}
	}, function(){
		if($button.hasClass('following')){
			$button.removeClass('unfollow');
			$button.text('Following');
		}
	});
	</script>
	
</body>
</html>